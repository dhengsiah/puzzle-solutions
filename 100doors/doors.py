class Doors:
    
    #state of doors: 0 = Closed, 1 = Opened
    state = 0

    def __init__(self,n):
        self.no = n
    
    def flip(self):
        self.state ^= 1
    
class DoorArray:

    doorlist = []
    def __init__(self,n):
        for i in range(0,n):
            self.doorlist.append(Doors(i+1))

    def flip_doors(self,n):
        
        if n == 0:
            return

        else:
            for x in self.doorlist:
                if x.no%n == 0:
                    x.flip()

            self.flip_doors(n-1)


    def print_door_state(self):
        opened = []
        closed = []
        for x in self.doorlist:
            if x.state == 1:
                opened.append(x.no)
            else:
                closed.append(x.no)

        print 'Opened doors are: ' + str(opened)
        print 'Closed doors are: ' + str(closed)

        return

if __name__ == "__main__":
    a = DoorArray(100)
    a.flip_doors(100)
    a.print_door_state()
